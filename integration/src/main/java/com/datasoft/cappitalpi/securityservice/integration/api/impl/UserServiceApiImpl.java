package com.datasoft.cappitalpi.securityservice.integration.api.impl;

import com.datasoft.cappitalpi.core.config.resource.MessageTranslator;
import com.datasoft.cappitalpi.core.exception.AppException;
import com.datasoft.cappitalpi.core.util.HeaderConstant;
import com.datasoft.cappitalpi.securityservice.integration.api.UserServiceApi;
import com.datasoft.cappitalpi.securityservice.integration.config.resource.ApiResource;
import com.datasoft.cappitalpi.securityservice.integration.config.resource.MessageIdResource;
import com.datasoft.cappitalpi.securityservice.integration.dto.UserDto;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.text.MessageFormat;

@Component
public class UserServiceApiImpl implements UserServiceApi {

    private final RestTemplate restTemplate;
    private final ApiResource apiResource;
    private final MessageTranslator messageTranslator;

    public UserServiceApiImpl(RestTemplate restTemplate, ApiResource apiResource, MessageTranslator messageTranslator) {
        this.restTemplate = restTemplate;
        this.apiResource = apiResource;
        this.messageTranslator = messageTranslator;
    }

    @Override
    public UserDto findByEmailOrUsername(String value) {
        if (!StringUtils.isBlank(value)) {
            try {
                final String endPoint = MessageFormat.format(apiResource.getUserService().getFindByEmailOrUsername(), value);
                ResponseEntity<UserDto> response = restTemplate.getForEntity(endPoint, UserDto.class);
                if (HttpStatus.OK.equals(response.getStatusCode())) {
                    return response.getBody();
                }
            } catch (HttpClientErrorException e) {
                if (HttpStatus.NOT_FOUND.equals(e.getStatusCode())) {
                    throw new AppException(messageTranslator.getMessage(MessageIdResource.Validation.INVALID_USER_OR_PASSWORD));
                }
            } catch (RestClientException e) {
                throw new AppException(
                        messageTranslator.getMessage(MessageIdResource.Validation.SOMETHING_WENT_WRONG_CALLING_SERVICE, apiResource.getUserService().getBaseUrl())
                );
            } catch (Exception e) {
                throw new AppException(
                        messageTranslator.getMessage(MessageIdResource.Validation.ERROR_CALLING_SERVICE, apiResource.getUserService().getBaseUrl())
                );
            }
        }
        return null;
    }

    @Override
    public UserDto findByUid(String uid, String registerUid) {
        if (StringUtils.isNotEmpty(uid) && StringUtils.isNotEmpty(registerUid)) {
            try {
                final MultiValueMap<String, String> headers = new HttpHeaders();
                headers.add(HeaderConstant.REGISTER_UID, registerUid);

                final HttpEntity<String> entity = new HttpEntity<>(headers);
                final String endPoint = MessageFormat.format(apiResource.getUserService().getFindByUid(), uid);

                ResponseEntity<UserDto> response = restTemplate.exchange(endPoint, HttpMethod.GET, entity, UserDto.class);
                if (HttpStatus.OK.equals(response.getStatusCode())) {
                    return response.getBody();
                }
            } catch (HttpClientErrorException e) {
                if (HttpStatus.NOT_FOUND.equals(e.getStatusCode())) {
                    throw new UsernameNotFoundException(
                            messageTranslator.getMessage(MessageIdResource.Validation.NON_EXISTING_OBJECT, "User", uid)
                    );
                }
            } catch (RestClientException e) {
                throw new AppException(
                        messageTranslator.getMessage(MessageIdResource.Validation.SOMETHING_WENT_WRONG_CALLING_SERVICE, apiResource.getUserService().getBaseUrl())
                );
            } catch (Exception e) {
                throw new AppException(
                        messageTranslator.getMessage(MessageIdResource.Validation.ERROR_CALLING_SERVICE, apiResource.getUserService().getBaseUrl())
                );
            }
        }
        return null;
    }
}
