package com.datasoft.cappitalpi.securityservice.application;

import com.datasoft.cappitalpi.securityservice.integration.config.SecurityConfig;
import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Import;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

@OpenAPIDefinition(info = @Info(title = "Security Service API", version = "1.0.1", description = "Security Information"))

@Import({SecurityConfig.class})
@EnableEncryptableProperties
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class, DataSourceAutoConfiguration.class})
public class Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        LOGGER.info("Starting at time {} / {}", LocalDateTime.now(), ZoneOffset.systemDefault().getId());
        SpringApplication.run(Application.class, args);
    }

}
