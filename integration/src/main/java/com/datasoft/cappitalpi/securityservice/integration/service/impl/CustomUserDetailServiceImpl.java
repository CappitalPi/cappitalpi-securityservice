package com.datasoft.cappitalpi.securityservice.integration.service.impl;

import com.datasoft.cappitalpi.securityservice.integration.api.UserServiceApi;
import com.datasoft.cappitalpi.securityservice.integration.config.JwtTokenProvider;
import com.datasoft.cappitalpi.securityservice.integration.dto.AuthResponseDto;
import com.datasoft.cappitalpi.securityservice.integration.dto.UserDto;
import com.datasoft.cappitalpi.securityservice.integration.dto.UserPrincipal;
import com.datasoft.cappitalpi.securityservice.integration.service.CustomUserDetailService;
import com.datasoft.cappitalpi.securityservice.integration.util.JwtUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

@Service
public class CustomUserDetailServiceImpl implements CustomUserDetailService {

    private final JwtTokenProvider tokenProvider;
    private final UserServiceApi userServiceApi;

    public CustomUserDetailServiceImpl(JwtTokenProvider tokenProvider, UserServiceApi userServiceApi) {
        this.tokenProvider = tokenProvider;
        this.userServiceApi = userServiceApi;
    }

    @Transactional
    @Override
    public UserDetails loadUserByUsername(String value) throws UsernameNotFoundException {
        UserDto user = userServiceApi.findByEmailOrUsername(value);
        return UserPrincipal.create(user);
    }

    @Override
    public AuthResponseDto validateAuth(String bearerToken) {
        String jwtToken = JwtUtils.getJwtFromRequest(bearerToken);
        boolean isTokenValid = StringUtils.hasText(jwtToken) && tokenProvider.validateToken(jwtToken);
        if (isTokenValid) {
            return new AuthResponseDto.Builder()
                    .setOwnerUid(tokenProvider.getClaim(jwtToken, JwtTokenProvider.USER_SESSION_OWNER_UID))
                    .setRegisterUid(tokenProvider.getClaim(jwtToken, JwtTokenProvider.USER_SESSION_REGISTER_UID))
                    .build();
        }
        return null;
    }

    @Transactional
    public UserDetails loadUserByUid(String uid, String registerUid) {
        UserDto user = userServiceApi.findByUid(uid, registerUid);
        return UserPrincipal.create(user);
    }

}