package com.datasoft.cappitalpi.securityservice.integration.config.resource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "api")
public class ApiResource {

    private final UserService userService = new UserService();

    public UserService getUserService() {
        return userService;
    }

    public static class UserService {

        private String baseUrl;
        private String findByEmailOrUsername;
        private String findByUid;

        public String getBaseUrl() {
            return baseUrl;
        }

        public void setBaseUrl(String baseUrl) {
            this.baseUrl = baseUrl;
        }

        public String getFindByEmailOrUsername() {
            return findByEmailOrUsername;
        }

        public void setFindByEmailOrUsername(String findByEmailOrUsername) {
            this.findByEmailOrUsername = findByEmailOrUsername;
        }

        public String getFindByUid() {
            return findByUid;
        }

        public void setFindByUid(String findByUid) {
            this.findByUid = findByUid;
        }
    }
}
