package com.datasoft.cappitalpi.securityservice.integration.util;

import com.google.common.truth.Truth;
import org.junit.jupiter.api.Test;

class JwtUtilsTest {

    @Test
    void givenNullToken_whenGet_thenReturnNull() {
        String jwtFromRequest = JwtUtils.getJwtFromRequest(null);
        Truth.assertThat(jwtFromRequest).isNull();
    }

    @Test
    void givenToken_whenGet_thenReturnToken() {
        String token = "Bearer test";
        String jwtFromRequest = JwtUtils.getJwtFromRequest(token);
        Truth.assertThat(jwtFromRequest).isNotNull();
        Truth.assertThat(jwtFromRequest).isEqualTo("test");
    }

}