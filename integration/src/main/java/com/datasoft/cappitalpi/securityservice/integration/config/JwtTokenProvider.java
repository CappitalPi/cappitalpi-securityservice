package com.datasoft.cappitalpi.securityservice.integration.config;

import com.datasoft.cappitalpi.securityservice.integration.dto.UserPrincipal;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class JwtTokenProvider {

    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

    public static final String USER_SESSION_REGISTER_UID = "registerUid";
    public static final String USER_SESSION_OWNERS = "owners";
    public static final String USER_SESSION_OWNER_UID = "ownerUid";
    public static final String USER_SESSION_CODE = "code";
    public static final String USER_SESSION_USERNAME = "username";
    public static final String USER_SESSION_AUTHORITIES = "acl";

    @Value("${app.jwt-secret}")
    private String jwtSecret;

    @Value("${app.jwt-expirationInMs}")
    private int jwtExpirationInMs;

    @Value("${app.jwt-audience}")
    private String jwtAudience;

    @Value("${app.jwt-issuer}")
    private String jwtIssuer;

    public String generateToken(Authentication authentication) {

        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();

        LocalDateTime nextDay = LocalDateTime.now()
                .plusDays(1)
                .withHour(1)
                .withMinute(0)
                .withSecond(0);
        Date nextExpiration = Date.from(nextDay.atZone(ZoneId.systemDefault()).toInstant());

        Map<String, Object> publicClaims = new HashMap<>();
        publicClaims.put(USER_SESSION_CODE, userPrincipal.getCode());
        publicClaims.put(USER_SESSION_USERNAME, userPrincipal.getUsername());
        publicClaims.put(USER_SESSION_REGISTER_UID, userPrincipal.getRegisterUid());
        publicClaims.put(USER_SESSION_OWNERS, userPrincipal.getOwnerUidList());
        publicClaims.put(USER_SESSION_OWNER_UID, CollectionUtils.firstElement(userPrincipal.getOwnerUidList()));

        return Jwts.builder()
                .setIssuer(jwtIssuer)
                .setSubject(userPrincipal.getUid())
                .setAudience(jwtAudience)
                .setExpiration(nextExpiration)
                .setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .addClaims(publicClaims)
                .compact();
    }

    public String getUserIdFromJwt(String token) {
        Claims claims = getClaims(token);
        return claims.getSubject();
    }

    public String getClaim(String token, String claimName) {
        Claims claims = getClaims(token);
        return claims.get(claimName, String.class);
    }

    private Claims getClaims(String token) {
        return Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody();
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException ex) {
            logger.error("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            logger.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            logger.error("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            logger.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            logger.error("JWT claims string is empty.");
        }
        return false;
    }
}