package com.datasoft.cappitalpi.securityservice.integration.controller;

import com.datasoft.cappitalpi.securityservice.integration.dto.AuthResponseDto;
import com.datasoft.cappitalpi.securityservice.integration.dto.JwtAuthenticationResponse;
import com.datasoft.cappitalpi.securityservice.integration.dto.LoginRequestDto;
import com.datasoft.cappitalpi.securityservice.integration.service.AuthService;
import com.datasoft.cappitalpi.securityservice.integration.service.CustomUserDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@RestController
@RequestMapping("/v1/auth")
public class AuthController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);

    private final AuthService authService;
    private final CustomUserDetailService customUserDetailService;

    public AuthController(AuthService authService, CustomUserDetailService customUserDetailService) {
        this.authService = authService;
        this.customUserDetailService = customUserDetailService;
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginRequestDto loginRequest) {
        LOGGER.info("Trying to connect with {}", loginRequest.getUsername());
        JwtAuthenticationResponse response = authService.authenticate(loginRequest);
        LOGGER.info("{} connected successfully!!", loginRequest.getUsername());
        return ResponseEntity.ok(response);
    }

    @PostMapping("/logout")
    public ResponseEntity<?> logout() {
        LOGGER.info("Trying to logout");
        authService.logout();
        LOGGER.info("Logout successfully!!");
        return ResponseEntity.ok().build();
    }

    @GetMapping("/authenticate")
    public ResponseEntity<AuthResponseDto> authenticate(@RequestHeader(value = AUTHORIZATION, required = false) String bearerToken) {
        LOGGER.info("Authenticating user...");
        AuthResponseDto response = customUserDetailService.validateAuth(bearerToken);
        if (response != null) {
            LOGGER.info("User successfully authenticated.");
            return ResponseEntity.ok(response);
        }
        LOGGER.info("Could not authenticated user. {}", HttpStatus.UNAUTHORIZED);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

}