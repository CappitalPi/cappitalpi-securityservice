package com.datasoft.cappitalpi.securityservice.integration.controller;

import com.datasoft.cappitalpi.securityservice.integration.DtoFactoryUtils;
import com.datasoft.cappitalpi.securityservice.integration.service.AuthService;
import com.datasoft.cappitalpi.securityservice.integration.service.CustomUserDetailService;
import com.google.common.truth.Truth;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class SecurityControllerTest {

    private AuthController authController;
    @Mock
    private AuthService authService;
    @Mock
    private CustomUserDetailService customUserDetailService;

    @BeforeEach
    void setUp() {
        authController = new AuthController(authService, customUserDetailService);
    }

    @Test
    void whenLogin_thenReturnOk() {
        when(authService.authenticate(any())).thenReturn(DtoFactoryUtils.jwtAuthenticationResponse());
        ResponseEntity<?> responseEntity = authController.login(DtoFactoryUtils.loginRequestDto());

        verify(authService, times(1)).authenticate(any());
        Truth.assertThat(responseEntity).isNotNull();
        Truth.assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void whenLogout_thenReturnOk() {
        ResponseEntity<?> responseEntity = authController.logout();
        verify(authService, times(1)).logout();
        Truth.assertThat(responseEntity).isNotNull();
        Truth.assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void whenAuthenticate_thenReturnNotAuthorized() {
        when(customUserDetailService.validateAuth(any())).thenReturn(null);
        ResponseEntity<?> responseEntity = authController.authenticate("any");

        verify(customUserDetailService, times(1)).validateAuth(any());
        Truth.assertThat(responseEntity).isNotNull();
        Truth.assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    void whenAuthenticate_thenReturnOk() {
        when(customUserDetailService.validateAuth(any())).thenReturn(DtoFactoryUtils.authResponseDto());
        ResponseEntity<?> responseEntity = authController.authenticate("any");

        verify(customUserDetailService, times(1)).validateAuth(any());
        Truth.assertThat(responseEntity).isNotNull();
        Truth.assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

}