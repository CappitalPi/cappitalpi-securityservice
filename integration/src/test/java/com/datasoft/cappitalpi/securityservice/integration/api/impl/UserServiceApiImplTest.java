package com.datasoft.cappitalpi.securityservice.integration.api.impl;

import com.datasoft.cappitalpi.core.config.resource.MessageTranslator;
import com.datasoft.cappitalpi.core.exception.AppException;
import com.datasoft.cappitalpi.securityservice.integration.DtoFactoryUtils;
import com.datasoft.cappitalpi.securityservice.integration.TestUtils;
import com.datasoft.cappitalpi.securityservice.integration.api.UserServiceApi;
import com.datasoft.cappitalpi.securityservice.integration.config.resource.ApiResource;
import com.datasoft.cappitalpi.securityservice.integration.config.resource.MessageIdResource;
import com.datasoft.cappitalpi.securityservice.integration.dto.UserDto;
import com.google.common.truth.Truth;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class UserServiceApiImplTest {

    private UserServiceApi userServiceApi;
    private ApiResource apiResource;
    @Mock
    private RestTemplate restTemplate;


    @BeforeEach
    void setup() {
        apiResource = TestUtils.ApiResource();
        MessageTranslator messageTranslator = TestUtils.messageTranslator();
        userServiceApi = new UserServiceApiImpl(restTemplate, apiResource, messageTranslator);
    }

    @Test
    void givenNullValue_whenFindByEmailOrUsername() {
        UserDto user = userServiceApi.findByEmailOrUsername(null);
        Truth.assertThat(user).isNull();
    }

    @Test
    void givenEmptyValue_whenFindByEmailOrUsername() {
        UserDto user = userServiceApi.findByEmailOrUsername(" ");
        Truth.assertThat(user).isNull();
    }

    @Test
    void givenUsername_whenFindByEmailOrUsername_thenThrowsRestClientException() {
        String value = "any";
        String endPoint = MessageFormat.format(apiResource.getUserService().getFindByEmailOrUsername(), value);
        when(restTemplate.getForEntity(eq(endPoint), eq(UserDto.class))).thenThrow(RestClientException.class);
        AppException e = assertThrows(AppException.class, () -> userServiceApi.findByEmailOrUsername(value));
        String message = TestUtils.formatMessage(MessageIdResource.Validation.SOMETHING_WENT_WRONG_CALLING_SERVICE, apiResource.getUserService().getBaseUrl());
        Truth.assertThat(e.getMessage()).isEqualTo(message);
    }

    @Test
    void givenUsername_whenFindByEmailOrUsername_thenThrowsHttpClientErrorException() {
        String value = "any";
        String endPoint = MessageFormat.format(apiResource.getUserService().getFindByEmailOrUsername(), value);
        when(restTemplate.getForEntity(eq(endPoint), eq(UserDto.class))).thenThrow(HttpClientErrorException.class);
        UserDto userDto = assertDoesNotThrow(() -> userServiceApi.findByEmailOrUsername(value));
        Truth.assertThat(userDto).isNull();
    }

    @Test
    void givenUsername_whenFindByEmailOrUsername_thenThrows404() {
        String value = "any";
        String endPoint = MessageFormat.format(apiResource.getUserService().getFindByEmailOrUsername(), value);
        when(restTemplate.getForEntity(eq(endPoint), eq(UserDto.class))).thenThrow(new HttpClientErrorException(HttpStatus.NOT_FOUND, "any", null, StandardCharsets.UTF_8));
        AppException e = assertThrows(AppException.class, () -> userServiceApi.findByEmailOrUsername(value));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.INVALID_USER_OR_PASSWORD));
    }

    @Test
    void givenUsername_whenFindByEmailOrUsername_thenThrowsException() {
        String value = "any";
        String endPoint = MessageFormat.format(apiResource.getUserService().getFindByEmailOrUsername(), value);
        when(restTemplate.getForEntity(eq(endPoint), eq(UserDto.class))).thenThrow(NullPointerException.class);
        AppException e = assertThrows(AppException.class, () -> userServiceApi.findByEmailOrUsername(value));
        String message = TestUtils.formatMessage(MessageIdResource.Validation.ERROR_CALLING_SERVICE, apiResource.getUserService().getBaseUrl());
        Truth.assertThat(e.getMessage()).isEqualTo(message);
    }

    @Test
    void givenUsername_whenFindByEmailOrUsername_thenReturnUser() {
        String value = "any";
        String endPoint = MessageFormat.format(apiResource.getUserService().getFindByEmailOrUsername(), value);
        ResponseEntity<UserDto> response = new ResponseEntity<>(DtoFactoryUtils.userDto(), HttpStatus.OK);
        when(restTemplate.getForEntity(eq(endPoint), eq(UserDto.class))).thenReturn(response);
        UserDto userDto = assertDoesNotThrow(() -> userServiceApi.findByEmailOrUsername(value));
        Truth.assertThat(userDto).isNotNull();
    }

    @Test
    void givenParameters_whenFindByUid_thenThrowsRestClientException() {
        String value = "any";
        String endPoint = MessageFormat.format(apiResource.getUserService().getFindByUid(), value);
        when(restTemplate.exchange(eq(endPoint), eq(HttpMethod.GET), any(), eq(UserDto.class))).thenThrow(RestClientException.class);
        AppException e = assertThrows(AppException.class, () -> userServiceApi.findByUid(TestUtils.newUid(), TestUtils.newUid()));
        String message = TestUtils.formatMessage(MessageIdResource.Validation.SOMETHING_WENT_WRONG_CALLING_SERVICE, apiResource.getUserService().getBaseUrl());
        Truth.assertThat(e.getMessage()).isEqualTo(message);
    }

    @Test
    void givenParameters_whenFindByUid_thenThrowsHttpClientErrorException() {
        String value = "any";
        String endPoint = MessageFormat.format(apiResource.getUserService().getFindByUid(), value);
        when(restTemplate.exchange(eq(endPoint), eq(HttpMethod.GET), any(), eq(UserDto.class))).thenThrow(HttpClientErrorException.class);
        UserDto userDto = assertDoesNotThrow(() -> userServiceApi.findByUid(TestUtils.newUid(), TestUtils.newUid()));
        Truth.assertThat(userDto).isNull();
    }

    @Test
    void givenParameters_whenFindByUid_thenThrows404() {
        String value = "any";
        String endPoint = MessageFormat.format(apiResource.getUserService().getFindByUid(), value);
        when(restTemplate.exchange(eq(endPoint), eq(HttpMethod.GET), any(), eq(UserDto.class))).thenThrow(new HttpClientErrorException(HttpStatus.NOT_FOUND, "any", null, StandardCharsets.UTF_8));
        String uid = TestUtils.newUid();
        UsernameNotFoundException e = assertThrows(UsernameNotFoundException.class, () -> userServiceApi.findByUid(uid, TestUtils.newUid()));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.NON_EXISTING_OBJECT, "User", uid));
    }

    @Test
    void givenParameters_whenFindByUid_thenThrowsException() {
        String value = "any";
        String endPoint = MessageFormat.format(apiResource.getUserService().getFindByUid(), value);
        when(restTemplate.exchange(eq(endPoint), eq(HttpMethod.GET), any(), eq(UserDto.class))).thenThrow(NullPointerException.class);
        AppException e = assertThrows(AppException.class, () -> userServiceApi.findByUid(TestUtils.newUid(), TestUtils.newUid()));
        String message = TestUtils.formatMessage(MessageIdResource.Validation.ERROR_CALLING_SERVICE, apiResource.getUserService().getBaseUrl());
        Truth.assertThat(e.getMessage()).isEqualTo(message);
    }

    @Test
    void givenParameters_whenFindByUid_thenReturnUser() {
        String value = "any";
        String endPoint = MessageFormat.format(apiResource.getUserService().getFindByUid(), value);
        ResponseEntity<UserDto> response = new ResponseEntity<>(DtoFactoryUtils.userDto(), HttpStatus.OK);
        when(restTemplate.exchange(eq(endPoint), eq(HttpMethod.GET), any(), eq(UserDto.class))).thenReturn(response);
        UserDto userDto = assertDoesNotThrow(() -> userServiceApi.findByUid(TestUtils.newUid(), TestUtils.newUid()));
        Truth.assertThat(userDto).isNotNull();
    }

}