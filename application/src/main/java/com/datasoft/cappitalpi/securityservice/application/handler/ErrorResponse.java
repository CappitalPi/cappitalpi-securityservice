package com.datasoft.cappitalpi.securityservice.application.handler;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ErrorResponse {

    private static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    private String date;
    private int status;
    private String message;
    private String details;

    public ErrorResponse() {
    }

    public ErrorResponse(Date date, int status, String message, String details) {
        this.date = new SimpleDateFormat(DATE_TIME_FORMAT).format(date);
        this.status = status;
        this.message = message;
        this.details = details;
    }

    public ErrorResponse(int status, String message, String details) {
        this.date = new SimpleDateFormat(DATE_TIME_FORMAT).format(new Date());
        this.status = status;
        this.message = message;
        this.details = details;
    }

    public String getDate() {
        return date;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public String getDetails() {
        return details;
    }

}