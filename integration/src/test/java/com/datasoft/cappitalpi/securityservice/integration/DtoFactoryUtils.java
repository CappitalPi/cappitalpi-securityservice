package com.datasoft.cappitalpi.securityservice.integration;

import com.datasoft.cappitalpi.securityservice.integration.dto.AuthResponseDto;
import com.datasoft.cappitalpi.securityservice.integration.dto.JwtAuthenticationResponse;
import com.datasoft.cappitalpi.securityservice.integration.dto.LoginRequestDto;
import com.datasoft.cappitalpi.securityservice.integration.dto.UserDto;
import org.apache.commons.lang3.RandomStringUtils;

import java.time.LocalDateTime;

public class DtoFactoryUtils {

    public static JwtAuthenticationResponse jwtAuthenticationResponse() {
        JwtAuthenticationResponse token = new JwtAuthenticationResponse("any");
        token.setTokenType("Bearer");
        return token;
    }

    public static LoginRequestDto loginRequestDto() {
        LoginRequestDto loginRequestDto = new LoginRequestDto();
        loginRequestDto.setPassword("any");
        loginRequestDto.setUsername("user");
        return loginRequestDto;
    }

    public static UserDto userDto() {
        UserDto userDto = new UserDto();
        userDto.setUid(TestUtils.newUid());
        userDto.setCode(TestUtils.newId());
        userDto.setRegisterUid(TestUtils.newUid());
        userDto.setUsername(RandomStringUtils.randomAlphabetic(10));
        userDto.setPassword(RandomStringUtils.randomAlphabetic(10));
        userDto.setEmail(RandomStringUtils.randomAlphabetic(10));
        userDto.setDisabled(false);
        userDto.setLastPasswordDate(LocalDateTime.now());
        userDto.setCreatedDate(LocalDateTime.now());
        userDto.setUpdatedDate(LocalDateTime.now());
        userDto.setDisabledDate(LocalDateTime.now());
        return userDto;
    }

    public static AuthResponseDto authResponseDto() {
        return new AuthResponseDto.Builder()
                .setOwnerUid(TestUtils.newUid())
                .setRegisterUid(TestUtils.newUid())
                .build();
    }
}
