package com.datasoft.cappitalpi.securityservice.application.handler;

import com.google.common.truth.Truth;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

class ErrorResponseTest {

    @Test
    public void givenValidDate_whenCreateErrorResponseInstance_thenReturnErrorResponse() throws ParseException {
        final Date date = Date.from(LocalDateTime.now().plusDays(10).toInstant(ZoneOffset.UTC));
        final int status = 2;
        final String message = RandomStringUtils.randomAlphabetic(20);
        final String details = RandomStringUtils.randomAlphabetic(15);
        ErrorResponse errorResponse = new ErrorResponse(date, status, message, details);

        Date responseDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(errorResponse.getDate());
        final LocalDateTime localDateTimeResponse = LocalDateTime.ofInstant(responseDate.toInstant(), ZoneOffset.UTC);
        final LocalDateTime localDateTime = LocalDateTime.ofInstant(date.toInstant(), ZoneOffset.UTC);

        Truth.assertThat(localDateTimeResponse.getYear()).isEqualTo(localDateTime.getYear());
        Truth.assertThat(localDateTimeResponse.getMonthValue()).isEqualTo(localDateTime.getMonthValue());
        Truth.assertThat(localDateTimeResponse.getDayOfMonth()).isEqualTo(localDateTime.getDayOfMonth());
        Truth.assertThat(localDateTimeResponse.getHour()).isEqualTo(localDateTime.getHour());
        Truth.assertThat(localDateTimeResponse.getMinute()).isEqualTo(localDateTime.getMinute());
        Truth.assertThat(localDateTimeResponse.getSecond()).isEqualTo(localDateTime.getSecond());
        Truth.assertThat(errorResponse.getStatus()).isEqualTo(status);
        Truth.assertThat(errorResponse.getMessage()).isEqualTo(message);
        Truth.assertThat(errorResponse.getDetails()).isEqualTo(details);
    }

    @Test
    public void givenNoDate_whenCreateErrorResponseInstance_thenReturnErrorResponse() throws ParseException {
        final LocalDateTime currentDateTime = LocalDateTime.ofInstant(new Date().toInstant(), ZoneOffset.UTC);
        final int status = 2;
        final String message = RandomStringUtils.randomAlphabetic(20);
        final String details = RandomStringUtils.randomAlphabetic(15);
        ErrorResponse errorResponse = new ErrorResponse(status, message, details);

        Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(errorResponse.getDate());
        final LocalDateTime responseDate = LocalDateTime.ofInstant(date.toInstant(), ZoneOffset.UTC);

        Truth.assertThat(responseDate.getYear()).isEqualTo(currentDateTime.getYear());
        Truth.assertThat(responseDate.getMonthValue()).isEqualTo(currentDateTime.getMonthValue());
        Truth.assertThat(responseDate.getDayOfMonth()).isEqualTo(currentDateTime.getDayOfMonth());
        Truth.assertThat(errorResponse.getStatus()).isEqualTo(status);
        Truth.assertThat(errorResponse.getMessage()).isEqualTo(message);
        Truth.assertThat(errorResponse.getDetails()).isEqualTo(details);
    }

    @Test
    public void givenEmptyParameters_whenCreateErrorResponseInstance_thenFieldsAreNull() {
        ErrorResponse errorResponse = new ErrorResponse();
        Truth.assertThat(errorResponse.getDate()).isNull();
        Truth.assertThat(errorResponse.getStatus()).isEqualTo(0);
        Truth.assertThat(errorResponse.getMessage()).isNull();
        Truth.assertThat(errorResponse.getDetails()).isNull();

    }

}