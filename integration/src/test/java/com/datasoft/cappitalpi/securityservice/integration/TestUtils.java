package com.datasoft.cappitalpi.securityservice.integration;

import com.datasoft.cappitalpi.core.config.CoreConfig;
import com.datasoft.cappitalpi.core.config.resource.MessageTranslator;
import com.datasoft.cappitalpi.core.util.UidUtils;
import com.datasoft.cappitalpi.securityservice.integration.config.SecurityConfig;
import com.datasoft.cappitalpi.securityservice.integration.config.resource.ApiResource;
import org.springframework.context.MessageSource;

import java.util.Random;

public final class TestUtils {

    private TestUtils() {
    }

    public static Long newId() {
        int id = new Random().nextInt(Integer.MAX_VALUE);
        return (long) id;
    }

    public static String newUid() {
        return UidUtils.getEncId(newId());
    }

    public static MessageTranslator messageTranslator() {
        return new MessageTranslator(messageSource());
    }

    public static MessageSource messageSource() {
        return new SecurityConfig(new CoreConfig()).messageSource();
    }

    public static ApiResource ApiResource() {
        ApiResource apiResource = new ApiResource();
        apiResource.getUserService().setBaseUrl("any-base-url");
        apiResource.getUserService().setFindByUid("any-find-by-uid");
        apiResource.getUserService().setFindByEmailOrUsername("find-by-email-or-username/{0}");
        return apiResource;
    }

    public static String formatMessage(String messageId, String... params) {
        MessageTranslator messageTranslator = messageTranslator();
        return messageTranslator.getMessage(messageId, params);
    }

}
