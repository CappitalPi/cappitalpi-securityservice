package com.datasoft.cappitalpi.securityservice.integration.service.impl;

import com.datasoft.cappitalpi.securityservice.integration.config.JwtTokenProvider;
import com.datasoft.cappitalpi.securityservice.integration.dto.JwtAuthenticationResponse;
import com.datasoft.cappitalpi.securityservice.integration.dto.LoginRequestDto;
import com.datasoft.cappitalpi.securityservice.integration.service.AuthService;
import com.google.common.truth.Truth;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AuthServiceImplTest {

    private AuthService authService;
    @Mock
    private AuthenticationManager authenticationManager;
    @Mock
    private JwtTokenProvider tokenProvider;

    @BeforeEach
    void setup() {
        authService = new AuthServiceImpl(authenticationManager, tokenProvider);
    }

    @Test
    void givenLoginRequest_whenAuthenticate_thenIsAuthenticated() {
        Authentication authenticationMock = mock(Authentication.class);
        when(tokenProvider.generateToken(any())).thenReturn("token");
        when(authenticationManager.authenticate(any())).thenReturn(authenticationMock);
        JwtAuthenticationResponse authenticate = Assertions.assertDoesNotThrow(() -> authService.authenticate(new LoginRequestDto()));
        Truth.assertThat(authenticate).isNotNull();
        Truth.assertThat(authenticate.getAccessToken()).isEqualTo("token");
        verify(tokenProvider, times(1)).generateToken(any());
    }

    @Test
    void whenLogout_thenLogout() {
        Assertions.assertDoesNotThrow(() -> authService.logout());
    }

}