package com.datasoft.cappitalpi.securityservice.integration.filter;

import com.datasoft.cappitalpi.securityservice.integration.config.JwtTokenProvider;
import com.datasoft.cappitalpi.securityservice.integration.dto.UserPrincipal;
import com.datasoft.cappitalpi.securityservice.integration.service.CustomUserDetailService;
import com.google.common.truth.Truth;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class JwtAuthenticationFilterTest {

    private JwtAuthenticationFilter authenticationFilter;
    @Mock
    private JwtTokenProvider tokenProvider;
    @Mock
    private CustomUserDetailService customUserDetailService;
    @Mock
    private HttpServletRequest request;
    @Mock
    private FilterChain filterChain;

    @BeforeEach
    void setup() {
        authenticationFilter = new JwtAuthenticationFilter(tokenProvider, customUserDetailService);
    }

    @Test
    void givenEmptyToken_whenFilter_thenDoFilter() throws ServletException, IOException {
        MockHttpServletResponse mockHttpServletResponse = new MockHttpServletResponse();
        when(request.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn(StringUtils.EMPTY);

        Assertions.assertDoesNotThrow(() -> authenticationFilter.doFilterInternal(request, mockHttpServletResponse, filterChain));
        Truth.assertThat(mockHttpServletResponse.getStatus()).isEqualTo(HttpServletResponse.SC_OK);

        verify(filterChain, times(1)).doFilter(any(), any());
        verify(tokenProvider, times(0)).getUserIdFromJwt(any());
        verify(customUserDetailService, times(0)).loadUserByUid(any(), any());
    }

    @Test
    void whenFilter_thenThrowException() throws ServletException, IOException {
        MockHttpServletResponse mockHttpServletResponse = new MockHttpServletResponse();
        when(tokenProvider.validateToken(any())).thenThrow(NullPointerException.class);
        when(request.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn("Bearer token");

        Assertions.assertDoesNotThrow(() -> authenticationFilter.doFilterInternal(request, mockHttpServletResponse, filterChain));
        Truth.assertThat(mockHttpServletResponse.getStatus()).isEqualTo(HttpServletResponse.SC_FORBIDDEN);

        verify(filterChain, times(1)).doFilter(any(), any());
        verify(tokenProvider, times(0)).getUserIdFromJwt(any());
        verify(customUserDetailService, times(0)).loadUserByUid(any(), any());
    }

    @Test
    void whenFilter_thenDoFilter() throws ServletException, IOException {
        UserPrincipal userPrincipal = new UserPrincipal("uid", 10L, "any-username", "any-register-uid", null, null, null);
        MockHttpServletResponse mockHttpServletResponse = new MockHttpServletResponse();
        when(tokenProvider.validateToken(any())).thenReturn(true);
        when(request.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn("Bearer token");
        when(tokenProvider.getUserIdFromJwt(any())).thenReturn("user-uid");
        when(customUserDetailService.loadUserByUid(any(), any())).thenReturn(userPrincipal);

        Assertions.assertDoesNotThrow(() -> authenticationFilter.doFilterInternal(request, mockHttpServletResponse, filterChain));
        Truth.assertThat(mockHttpServletResponse.getStatus()).isEqualTo(HttpServletResponse.SC_OK);

        verify(filterChain, times(1)).doFilter(any(), any());
        verify(tokenProvider, times(1)).getUserIdFromJwt(any());
        verify(customUserDetailService, times(1)).loadUserByUid(any(), any());
    }

}