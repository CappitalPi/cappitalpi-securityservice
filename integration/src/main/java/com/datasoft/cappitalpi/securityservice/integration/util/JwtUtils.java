package com.datasoft.cappitalpi.securityservice.integration.util;

import org.springframework.util.StringUtils;

public final class JwtUtils {

    public static final String BEARER = "Bearer";

    private JwtUtils() {
    }

    public static String getJwtFromRequest(String bearerToken) {
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith(BEARER + " ")) {
            return bearerToken.substring(7);
        }
        return null;
    }
}