package com.datasoft.cappitalpi.securityservice.integration.dto;

public class AuthResponseDto {

    private final String ownerUid;
    private final String registerUid;

    public String getOwnerUid() {
        return ownerUid;
    }

    public String getRegisterUid() {
        return registerUid;
    }

    private AuthResponseDto(String ownerUid, String registerUid) {
        this.ownerUid = ownerUid;
        this.registerUid = registerUid;
    }

    public static class Builder {

        private String ownerUid;
        private String registerUid;

        public Builder setOwnerUid(String ownerUid) {
            this.ownerUid = ownerUid;
            return this;
        }

        public Builder setRegisterUid(String registerUid) {
            this.registerUid = registerUid;
            return this;
        }

        public AuthResponseDto build() {
            return new AuthResponseDto(ownerUid, registerUid);
        }
    }
}