package com.datasoft.cappitalpi.securityservice.application.handler;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.datasoft.cappitalpi.core.exception.ForbiddenException;
import com.datasoft.cappitalpi.core.exception.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.sql.SQLIntegrityConstraintViolationException;

@ControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandlerController.class);

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<ErrorResponse> handleServiceException(Exception ex, WebRequest request) {
        final HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        ErrorResponse error = new ErrorResponse(status.value(), "Server error.", request.getDescription(false));
        LOGGER.error(ex.getMessage(), ex);
        return new ResponseEntity<>(error, status);
    }

    @ExceptionHandler(InternalAuthenticationServiceException.class)
    public final ResponseEntity<ErrorResponse> handleInternalAuthenticationServiceException(InternalAuthenticationServiceException ex, WebRequest request) {
        final HttpStatus status = HttpStatus.UNAUTHORIZED;
        ErrorResponse error = new ErrorResponse(status.value(), ex.getMessage(), request.getDescription(false));
        LOGGER.error(ex.getMessage(), ex);
        return new ResponseEntity<>(error, status);
    }

    @ExceptionHandler(ValidationException.class)
    public final ResponseEntity<ErrorResponse> handleValidationServiceException(ValidationException ex, WebRequest request) {
        final HttpStatus status = HttpStatus.PRECONDITION_REQUIRED;
        ErrorResponse error = new ErrorResponse(status.value(), ex.getMessage(), request.getDescription(false));
        LOGGER.error(ex.getMessage(), ex);
        return new ResponseEntity<>(error, status);
    }

    @ExceptionHandler(ForbiddenException.class)
    public final ResponseEntity<ErrorResponse> handleForbiddenException(ForbiddenException ex, WebRequest request) {
        final HttpStatus status = HttpStatus.FORBIDDEN;
        ErrorResponse error = new ErrorResponse(status.value(), ex.getMessage(), request.getDescription(false));
        LOGGER.error(ex.getMessage(), ex);
        return new ResponseEntity<>(error, status);
    }

    @ExceptionHandler(BadCredentialsException.class)
    public final ResponseEntity<ErrorResponse> handleBadCredentials(BadCredentialsException ex, WebRequest request) {
        final HttpStatus status = HttpStatus.UNAUTHORIZED;
        ErrorResponse error = new ErrorResponse(status.value(), ex.getMessage(), request.getDescription(false));
        LOGGER.error(ex.getMessage(), ex);
        return new ResponseEntity<>(error, status);
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    public final ResponseEntity<ErrorResponse> handleUsernameNotFound(UsernameNotFoundException ex, WebRequest request) {
        final HttpStatus status = HttpStatus.NOT_FOUND;
        ErrorResponse error = new ErrorResponse(status.value(), ex.getMessage(), request.getDescription(false));
        LOGGER.error(ex.getMessage(), ex);
        return new ResponseEntity<>(error, status);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public final ResponseEntity<ErrorResponse> handleValidationServiceException(DataIntegrityViolationException ex, WebRequest request) {
        final HttpStatus status = HttpStatus.PRECONDITION_REQUIRED;
        String message = ex.getMessage();
        if (ex.getMostSpecificCause() instanceof SQLIntegrityConstraintViolationException) {
            message = ex.getMostSpecificCause().getMessage();
        }
        ErrorResponse error = new ErrorResponse(status.value(), message, request.getDescription(false));
        LOGGER.error(message, ex);
        return new ResponseEntity<>(error, status);
    }

    @ExceptionHandler(JsonMappingException.class)
    public final ResponseEntity<ErrorResponse> handleJsonMappingException(JsonMappingException ex, WebRequest request) {
        final ErrorResponse errorResponse = new ErrorResponse(HttpStatus.PRECONDITION_FAILED.value(), ex.getMessage(), request.getDescription(false));
        LOGGER.error(ex.getMessage(), ex);
        return new ResponseEntity<>(errorResponse, HttpStatus.PRECONDITION_FAILED);
    }

}

