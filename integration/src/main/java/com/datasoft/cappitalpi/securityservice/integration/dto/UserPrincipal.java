package com.datasoft.cappitalpi.securityservice.integration.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.datasoft.cappitalpi.core.util.UidUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class UserPrincipal implements UserDetails {

    private String uid;
    private final Long code;
    private final String username;
    private final String registerUid;
    private final List<String> ownerUidList;
    @JsonIgnore
    private final String email;
    @JsonIgnore
    private final String password;

    public UserPrincipal(String uid, Long code, String username, String registerUid, List<String> ownerUidList, String email, String password) {
        this.uid = uid;
        this.code = code;
        this.username = username;
        this.registerUid = registerUid;
        this.ownerUidList = ownerUidList;
        this.email = email;
        this.password = password;
    }

    public static UserPrincipal create(UserDto user) {
        return new UserPrincipal(
                user.getUid(),
                user.getCode(),
                user.getUsername(),
                user.getRegisterUid(),
                user.getOwnerUidList(),
                user.getEmail(),
                user.getPassword()
        );
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Long getCode() {
        return code;
    }

    public String getEmail() {
        return email;
    }

    public String getRegisterUid() {
        return registerUid;
    }

    public List<String> getOwnerUidList() {
        return ownerUidList;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserPrincipal that = (UserPrincipal) o;
        return Objects.equals(UidUtils.getDecId(uid), UidUtils.getDecId(that.uid));
    }

    @Override
    public int hashCode() {
        return Objects.hash(UidUtils.getDecId(uid));
    }

}