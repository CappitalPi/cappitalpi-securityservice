package com.datasoft.cappitalpi.securityservice.integration.filter;

import com.datasoft.cappitalpi.securityservice.integration.config.JwtTokenProvider;
import com.datasoft.cappitalpi.securityservice.integration.service.CustomUserDetailService;
import com.datasoft.cappitalpi.securityservice.integration.util.JwtUtils;
import io.micrometer.core.lang.NonNullApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@NonNullApi
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(JwtAuthenticationFilter.class);

    private final JwtTokenProvider tokenProvider;
    private final CustomUserDetailService customUserDetailService;

    private static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationFilter.class);

    public JwtAuthenticationFilter(JwtTokenProvider tokenProvider, CustomUserDetailService customUserDetailService) {
        this.tokenProvider = tokenProvider;
        this.customUserDetailService = customUserDetailService;
    }


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            LOGGER.info("Starting filter...");
            String jwt = JwtUtils.getJwtFromRequest(request.getHeader(HttpHeaders.AUTHORIZATION));
            if (StringUtils.hasText(jwt) && tokenProvider.validateToken(jwt)) {
                LOGGER.info("Token successfully validated.");
                final String uid = tokenProvider.getUserIdFromJwt(jwt);
                UserDetails userDetails = customUserDetailService.loadUserByUid(uid, tokenProvider.getClaim(jwt, JwtTokenProvider.USER_SESSION_REGISTER_UID));

                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                LOGGER.info("Setting authentication context.");
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        } catch (Exception ex) {
            logger.error("Could not set user authentication in security context", ex);
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
        }
        LOGGER.info("Do filter...");
        filterChain.doFilter(request, response);
    }

}