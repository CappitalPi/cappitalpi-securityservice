package com.datasoft.cappitalpi.securityservice.integration.dto;

import java.time.LocalDateTime;
import java.util.List;

public class UserDto {

    private String uid;
    private Long code;
    private String registerUid;
    private UserTypeDto type;
    private String username;
    private String password;
    private String email;
    private boolean disabled;
    private LocalDateTime lastPasswordDate;
    private LocalDateTime createdDate;
    private LocalDateTime updatedDate;
    private LocalDateTime disabledDate;
    private List<String> ownerUidList;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getRegisterUid() {
        return registerUid;
    }

    public void setRegisterUid(String registerUid) {
        this.registerUid = registerUid;
    }

    public UserTypeDto getType() {
        return type;
    }

    public void setType(UserTypeDto type) {
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public LocalDateTime getLastPasswordDate() {
        return lastPasswordDate;
    }

    public void setLastPasswordDate(LocalDateTime lastPasswordDate) {
        this.lastPasswordDate = lastPasswordDate;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    public LocalDateTime getDisabledDate() {
        return disabledDate;
    }

    public void setDisabledDate(LocalDateTime disabledDate) {
        this.disabledDate = disabledDate;
    }

    public List<String> getOwnerUidList() {
        return ownerUidList;
    }

    public void setOwnerUidList(List<String> ownerUidList) {
        this.ownerUidList = ownerUidList;
    }
}
