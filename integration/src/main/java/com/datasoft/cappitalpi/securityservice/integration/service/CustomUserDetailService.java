package com.datasoft.cappitalpi.securityservice.integration.service;

import com.datasoft.cappitalpi.securityservice.integration.dto.AuthResponseDto;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface CustomUserDetailService extends UserDetailsService {

    AuthResponseDto validateAuth(String bearerToken);

    UserDetails loadUserByUid(String uid, String registerUid);

}