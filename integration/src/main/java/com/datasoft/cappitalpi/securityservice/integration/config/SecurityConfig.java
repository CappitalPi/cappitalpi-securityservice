package com.datasoft.cappitalpi.securityservice.integration.config;


import com.datasoft.cappitalpi.core.config.CoreConfig;
import com.datasoft.cappitalpi.core.config.I18NConfig;
import com.datasoft.cappitalpi.securityservice.integration.config.resource.ApiResource;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.client.RestTemplate;

@Configuration
@EnableConfigurationProperties(ApiResource.class)
@ComponentScan(basePackages = "com.datasoft.cappitalpi.securityservice.integration")
@Import({CoreConfig.class, I18NConfig.class, WebSecurityConfig.class, AuditingConfig.class})
public class SecurityConfig {

    private final ReloadableResourceBundleMessageSource messageSource;

    public SecurityConfig(CoreConfig coreConfig) {
        this.messageSource = (ReloadableResourceBundleMessageSource) coreConfig.coreMessageSource();
        this.messageSource.addBasenames("classpath:messages/messages");
    }

    public MessageSource messageSource() {
        return messageSource;
    }

    @Bean
    public JwtTokenProvider tokenProvider() {
        return new JwtTokenProvider();
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}