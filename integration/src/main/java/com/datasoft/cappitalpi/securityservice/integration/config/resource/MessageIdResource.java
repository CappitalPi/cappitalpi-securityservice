package com.datasoft.cappitalpi.securityservice.integration.config.resource;

public final class MessageIdResource {

    public static final class Validation {
        public static final String NON_EXISTING_OBJECT = "message.validation.non-existing-object";
        public static final String INVALID_USER_OR_PASSWORD = "message.validation.invalid-user-or-password";
        public static final String SOMETHING_WENT_WRONG_CALLING_SERVICE = "message.validation.something-went-wrong-calling-service";
        public static final String ERROR_CALLING_SERVICE = "message.validation.error-calling-service";
    }
}
