package com.datasoft.cappitalpi.securityservice.application.handler;

import com.datasoft.cappitalpi.core.exception.ForbiddenException;
import com.datasoft.cappitalpi.core.exception.ValidationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.common.truth.Truth;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.context.request.WebRequest;

import java.sql.SQLIntegrityConstraintViolationException;

@ExtendWith(SpringExtension.class)
class ExceptionHandlerControllerTest {

    private static final String ERROR_MESSAGE = "Error Message";
    private static final String ERROR_MESSAGE_2 = "Error Message 02";
    @Mock
    private WebRequest request;
    private ExceptionHandlerController exceptionHandlerController;

    @BeforeEach
    void setUp() {
        exceptionHandlerController = new ExceptionHandlerController();
    }

    @Test
    void givenException_whenHandleThat_thenReturnResponse() {
        Exception exception = new Exception(ERROR_MESSAGE);
        ResponseEntity<ErrorResponse> response = exceptionHandlerController.handleServiceException(exception, request);
        Truth.assertThat(response).isNotNull();
        Truth.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        Truth.assertThat(response.getBody()).isNotNull();
        Truth.assertThat(response.getBody().getMessage()).contains("Server error.");
    }

    @Test
    void givenValidationException_whenHandleThat_thenReturnResponse() {
        ValidationException exception = new ValidationException(ERROR_MESSAGE);
        ResponseEntity<ErrorResponse> response = exceptionHandlerController.handleValidationServiceException(exception, request);
        Truth.assertThat(response).isNotNull();
        Truth.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.PRECONDITION_REQUIRED);
        Truth.assertThat(response.getBody()).isNotNull();
        Truth.assertThat(response.getBody().getMessage()).contains(exception.getMessage());
    }

    @Test
    void givenForbiddenException_whenHandleThat_thenReturnResponse() {
        ForbiddenException exception = new ForbiddenException(ERROR_MESSAGE);
        ResponseEntity<ErrorResponse> response = exceptionHandlerController.handleForbiddenException(exception, request);
        Truth.assertThat(response).isNotNull();
        Truth.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
        Truth.assertThat(response.getBody()).isNotNull();
        Truth.assertThat(response.getBody().getMessage()).contains(exception.getMessage());
    }

    @Test
    void givenInternalAuthenticationServiceException_whenHandleThat_thenReturnResponse() {
        InternalAuthenticationServiceException exception = new InternalAuthenticationServiceException(ERROR_MESSAGE);
        ResponseEntity<ErrorResponse> response = exceptionHandlerController.handleInternalAuthenticationServiceException(exception, request);
        Truth.assertThat(response).isNotNull();
        Truth.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
        Truth.assertThat(response.getBody()).isNotNull();
        Truth.assertThat(response.getBody().getMessage()).contains(exception.getMessage());
    }

    @Test
    void givenUsernameNotFoundException_whenHandleThat_thenReturnResponse() {
        UsernameNotFoundException exception = new UsernameNotFoundException(ERROR_MESSAGE);
        ResponseEntity<ErrorResponse> response = exceptionHandlerController.handleUsernameNotFound(exception, request);
        Truth.assertThat(response).isNotNull();
        Truth.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        Truth.assertThat(response.getBody()).isNotNull();
        Truth.assertThat(response.getBody().getMessage()).contains(exception.getMessage());
    }

    @Test
    void givenBadCredentialsException_whenHandleThat_thenReturnResponse() {
        BadCredentialsException exception = new BadCredentialsException(ERROR_MESSAGE);
        ResponseEntity<ErrorResponse> response = exceptionHandlerController.handleBadCredentials(exception, request);
        Truth.assertThat(response).isNotNull();
        Truth.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
        Truth.assertThat(response.getBody()).isNotNull();
        Truth.assertThat(response.getBody().getMessage()).contains(exception.getMessage());
    }

    @Test
    void givenDataIntegrityViolationException_whenHandleThat_thenReturnResponse() {
        DataIntegrityViolationException exception = new DataIntegrityViolationException(ERROR_MESSAGE);
        ResponseEntity<ErrorResponse> response = exceptionHandlerController.handleValidationServiceException(exception, request);
        Truth.assertThat(response).isNotNull();
        Truth.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.PRECONDITION_REQUIRED);
        Truth.assertThat(response.getBody()).isNotNull();
        Truth.assertThat(response.getBody().getMessage()).contains(exception.getMessage());
    }

    @Test
    void givenSQLIntegrityConstraintViolationException_whenHandleThat_thenReturnResponse() {
        SQLIntegrityConstraintViolationException throwable = new SQLIntegrityConstraintViolationException(ERROR_MESSAGE_2);
        DataIntegrityViolationException exception = new DataIntegrityViolationException(ERROR_MESSAGE, throwable);
        ResponseEntity<ErrorResponse> response = exceptionHandlerController.handleValidationServiceException(exception, request);
        Truth.assertThat(response).isNotNull();
        Truth.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.PRECONDITION_REQUIRED);
        Truth.assertThat(response.getBody()).isNotNull();
        Truth.assertThat(response.getBody().getMessage()).contains(exception.getMostSpecificCause().getMessage());
    }

    @Test
    void givenJsonMappingException_whenHandleThat_thenReturnResponse() {
        JsonMappingException exception = new JsonMappingException(null, ERROR_MESSAGE);
        ResponseEntity<ErrorResponse> response = exceptionHandlerController.handleJsonMappingException(exception, request);
        Truth.assertThat(response).isNotNull();
        Truth.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.PRECONDITION_FAILED);
        Truth.assertThat(response.getBody()).isNotNull();
        Truth.assertThat(response.getBody().getMessage()).contains(exception.getMessage());
    }

}