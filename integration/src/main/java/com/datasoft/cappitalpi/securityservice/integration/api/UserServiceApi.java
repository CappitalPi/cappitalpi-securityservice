package com.datasoft.cappitalpi.securityservice.integration.api;

import com.datasoft.cappitalpi.securityservice.integration.dto.UserDto;

public interface UserServiceApi {

    UserDto findByEmailOrUsername(String value);

    UserDto findByUid(String uid, String registerUid);
}
