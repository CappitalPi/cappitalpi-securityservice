package com.datasoft.cappitalpi.securityservice.integration.service.impl;

import com.datasoft.cappitalpi.securityservice.integration.api.UserServiceApi;
import com.datasoft.cappitalpi.securityservice.integration.config.JwtTokenProvider;
import com.datasoft.cappitalpi.securityservice.integration.dto.AuthResponseDto;
import com.datasoft.cappitalpi.securityservice.integration.dto.UserDto;
import com.datasoft.cappitalpi.securityservice.integration.service.CustomUserDetailService;
import com.google.common.truth.Truth;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CustomUserDetailServiceImplTest {

    private CustomUserDetailService customUserDetailService;
    @Mock
    private JwtTokenProvider tokenProvider;
    @Mock
    private UserServiceApi userServiceApi;

    @BeforeEach
    void setup() {
        customUserDetailService = new CustomUserDetailServiceImpl(tokenProvider, userServiceApi);
    }

    @Test
    void givenUserDto_whenLoadUserByUid_thenReturnUserDetails() {
        UserDto userDto = new UserDto();
        userDto.setUsername("any");
        when(userServiceApi.findByUid(any(), any())).thenReturn(userDto);
        UserDetails userDetails = Assertions.assertDoesNotThrow(() -> customUserDetailService.loadUserByUid("any", "any"));
        Truth.assertThat(userDetails).isNotNull();
        Truth.assertThat(userDetails.getUsername()).isEqualTo(userDto.getUsername());
    }

    @Test
    void givenUserDto_whenLoadByUsername_thenReturnUserDetails() {
        UserDto userDto = new UserDto();
        userDto.setUsername("any");
        when(userServiceApi.findByEmailOrUsername(any())).thenReturn(userDto);
        UserDetails userDetails = Assertions.assertDoesNotThrow(() -> customUserDetailService.loadUserByUsername("any"));
        Truth.assertThat(userDetails).isNotNull();
        Truth.assertThat(userDetails.getUsername()).isEqualTo(userDto.getUsername());
    }

    @Test
    void givenInvalidToken_whenValidateAuth_thenReturnNull() {
        when(tokenProvider.validateToken(any())).thenReturn(false);
        AuthResponseDto response = Assertions.assertDoesNotThrow(() -> customUserDetailService.validateAuth("Bearer token"));
        Truth.assertThat(response).isNull();
        verify(tokenProvider, times(0)).getClaim(any(), any());
    }

    @Test
    void givenEmptyToken_whenValidateAuth_thenReturnNull() {
        AuthResponseDto response = Assertions.assertDoesNotThrow(() -> customUserDetailService.validateAuth(" "));
        Truth.assertThat(response).isNull();
        verify(tokenProvider, times(0)).getClaim(any(), any());
    }

    @Test
    void givenValidToken_whenValidateAuth_thenReturnNull() {
        when(tokenProvider.validateToken(any())).thenReturn(true);
        when(tokenProvider.getClaim(any(), eq(JwtTokenProvider.USER_SESSION_OWNER_UID))).thenReturn("anyOwnerUid");
        when(tokenProvider.getClaim(any(), eq(JwtTokenProvider.USER_SESSION_REGISTER_UID))).thenReturn("anyRegisterUid");
        AuthResponseDto response = Assertions.assertDoesNotThrow(() -> customUserDetailService.validateAuth("Bearer token"));
        Truth.assertThat(response).isNotNull();
        verify(tokenProvider, times(2)).getClaim(any(), any());
    }

}