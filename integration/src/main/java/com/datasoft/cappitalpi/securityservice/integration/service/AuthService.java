package com.datasoft.cappitalpi.securityservice.integration.service;

import com.datasoft.cappitalpi.securityservice.integration.dto.JwtAuthenticationResponse;
import com.datasoft.cappitalpi.securityservice.integration.dto.LoginRequestDto;

public interface AuthService {

    JwtAuthenticationResponse authenticate(LoginRequestDto loginRequest);

    void logout();

}